@extends('layout.master')

@section('judul')
Data Pemain Film
@endsection

@section('content')
 
    <form action="{{ route('cast.update', ['cast_id' => $cast->id]) }}" method="post">
        @csrf
        @method('PUT')
        <label>Nama :</label> <br> <br>
        <input type="text" name="nama"> ,<br> <br> 
        @error('nama')
        <div class="alert alert-danger">
        {{ $message }}
        </div>
        @enderror

        <label>Umur :</label> <br> <br>
        <input type="number" name="umur"> ,<br> <br>
        @error('umur')
        <div class="alert alert-danger">
        {{ $message }}
        </div>
        @enderror
        <label>Bio :</label> <br> <br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea> <br>
        @error('bio')
        <div class="alert alert-danger">
        {{ $message }}
        </div>
        @enderror
        <input type="submit" value="Update">

@endsection